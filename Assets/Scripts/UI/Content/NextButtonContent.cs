﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Content
{
	//---------------------------------------------------------------------
	
	public class NextButtonContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[SerializeField]	Button		m_NextButton;
		
		//---------------------------------------------------------------------

		protected override void Start()
		{
			base.Start();

			if (m_NextButton != null)
			{
				m_NextButton.OnClickAsObservable().Subscribe
				(
					_ => GoNext()
				);
			}
		}
		
		//---------------------------------------------------------------------
	}
	
	//---------------------------------------------------------------------
}