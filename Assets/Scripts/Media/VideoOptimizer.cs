﻿using UnityEngine;
using UnityEngine.UI;

namespace Media
{
	[RequireComponent(typeof(RawImage))]
	[RequireComponent(typeof(MediaPlayerCtrl))]
	public class VideoOptimizer : MonoBehaviour 
	{
		//--------------------------------------------------------------------------------
		
		[SerializeField]		bool				m_PlayOnReady		=	true;
		[SerializeField]		string				m_VideoName;
		
		//--------------------------------------------------------------------------------
		
		private					RawImage			m_RawImage;
		private					MediaPlayerCtrl		m_MediaPlayerCtrl;
		
		//--------------------------------------------------------------------------------

		private void OnEnable()
		{
			if(m_MediaPlayerCtrl == null)
				m_MediaPlayerCtrl	=	GetComponent<MediaPlayerCtrl>();
			
			if(m_RawImage == null)
				m_RawImage	=	GetComponent<RawImage>();

			m_MediaPlayerCtrl.Load(m_VideoName);
			m_RawImage.enabled							=	false;
			m_MediaPlayerCtrl.OnReady					+=	OnReady;
			m_MediaPlayerCtrl.OnVideoFirstFrameReady	+=	OnFirstFrameReady;
		}
		
		//--------------------------------------------------------------------------------

		private void OnFirstFrameReady()
		{
			m_RawImage.enabled	=	true;
			
			if(!m_PlayOnReady)
				m_MediaPlayerCtrl.Pause();
		}
		
		//--------------------------------------------------------------------------------

		private void OnReady()
		{
			m_MediaPlayerCtrl.Play();
		}
		
		//--------------------------------------------------------------------------------

		private void OnDisable()
		{
			if(m_MediaPlayerCtrl == null)
				m_MediaPlayerCtrl	=	GetComponent<MediaPlayerCtrl>();
			
			if(m_RawImage == null)
				m_RawImage	=	GetComponent<RawImage>();
			
			m_MediaPlayerCtrl.Stop();
			m_MediaPlayerCtrl.UnLoad();
			m_RawImage.enabled							=	false;
			m_MediaPlayerCtrl.OnReady					-=	OnReady;
			m_MediaPlayerCtrl.OnVideoFirstFrameReady	-=	OnFirstFrameReady;
			
		}
	
		//--------------------------------------------------------------------------------
	}
}
