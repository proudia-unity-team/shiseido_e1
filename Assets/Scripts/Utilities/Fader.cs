using System;
using System.Security.Cryptography;
using UniRx;
using UnityEngine;

namespace Utilities
{
	//--------------------------------------------------------------------------------

	public class Fader
	{
		public enum Type
		{
			Linear,
			Pingpong
		}

		//--------------------------------------------------------------------------------

		public static IDisposable Animate(float _start, float _target, float _duration, Action<float> _ticker, Action _onEnd = null)
		{
			var	progress	=	new ReactiveProperty<float>(0f);
			var	result		=	Observable.EveryLateUpdate().Subscribe
			(
				_ =>
				{
					var	nextProgress	=	progress.Value + Time.deltaTime / _duration;

					_ticker
					(
						Mathf.Lerp
						(
							_start,
							_target,
							nextProgress
						)
					);

					progress.Value		=	nextProgress;
				}
			);

			progress.Where
				(
					_progress => _progress >= 1f
				).Subscribe
				(
					_ =>
					{
						result.Dispose();

						if(_onEnd != null)
							_onEnd();

						progress.Dispose();
					}
				);

			return	result;	
		}

		//--------------------------------------------------------------------------------

		public static IDisposable Animate(float _start, float _target, float _duration, Easing.Type _easeType, Action<float> _ticker, Action _onEnd = null)
		{
			var	progress	=	new ReactiveProperty<float>(0f);
			var	result		=	Observable.EveryLateUpdate().Subscribe
			(
				_ =>
				{
					var	nextProgress	=	progress.Value + Time.deltaTime / _duration;
					var	tickerValue		=	Easing.GetEasingFunction(_easeType).Invoke(_start, _target, nextProgress);

					_ticker
					(
						float.IsNaN(tickerValue) ? 1f : tickerValue 
					);

					progress.Value		=	float.IsNaN(tickerValue) ? 1f : nextProgress;
				}
			);

			progress.Where
				(
					_progress => _progress >= 1f
				).Subscribe
				(
					_ =>
					{
						result.Dispose();

						if(_onEnd != null)
							_onEnd();

						progress.Dispose();
					}
				);

			return	result;	
		}
	
		//--------------------------------------------------------------------------------
	}
}