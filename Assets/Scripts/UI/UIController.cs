﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI
{
	//---------------------------------------------------------------------
		
	public enum UIState
	{
		None			=	0,
		Standby			=	1,
		Selection		=	2,
		Warning			=	3,
		TakePhoto		=	4,
		HandTouch		=	5,
		Interactive		=	6,
		Summary			=	7
	}
	
	//---------------------------------------------------------------------
	
	public class UIController : Singleton<UIController>
	{
		//---------------------------------------------------------------------

		[Header("Content")]
		[SerializeField]	BackgroundContent				m_BackgroundContent;
		[SerializeField]	UIContent[]						m_UIContents;
		
		//---------------------------------------------------------------------
		
		private readonly 	Dictionary<UIState, UIContent>	m_ContentDictionary		=	new Dictionary<UIState, UIContent>();
		
		//---------------------------------------------------------------------

		private void Start () 
		{
			Reset();
		}
		
		//---------------------------------------------------------------------

		public T GetContent<T>() where T : UIContent
		{
			T result	=	null;
			
			for (var i = 0; i < m_UIContents.Length; i++)
			{
				if (m_UIContents[i] is T)
				{
					result = (T) Convert.ChangeType(m_UIContents[i], typeof(T));
					break;
				}
			}
			
			return result;
		}
		//---------------------------------------------------------------------

		public UIContent GetContent(UIState _state)
		{
			return m_ContentDictionary.ContainsKey(_state) ? m_ContentDictionary[_state] : null;
		}
		
		//---------------------------------------------------------------------

		[ContextMenu("Reset")]
		public void Reset()
		{
			m_BackgroundContent.Init(0, true, null, null);
			SetBackgroundColor(BackgroundColor.White);
				
			if (m_UIContents.Length > 0)
			{
				for (var i = 0; i < m_UIContents.Length; i++)
				{
					if (m_ContentDictionary.ContainsKey(m_UIContents[i].GetState()))
						continue;
					
					m_ContentDictionary.Add(m_UIContents[i].GetState(), m_UIContents[i]);
				}
				
				for (var i = 0; i < m_UIContents.Length; i++)
				{
					m_UIContents[i].Init
					(
						i + 1,
						i == 0,
						i < m_UIContents.Length - 1 ?  m_UIContents[i+1] : null,
						i == 0 ? null : m_UIContents[i-1]
				   );
				}
			}
		}
		
		//---------------------------------------------------------------------

		public void SetBackgroundColor(BackgroundColor _color)
		{
			m_BackgroundContent.SetBackgroundColor(_color);
		}
		
		//---------------------------------------------------------------------

		private void Quit()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif				
		}
		
		//---------------------------------------------------------------------
	}
}
