﻿using Controller;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI.Content
{
	//---------------------------------------------------------------------

	public class InteractiveContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[Header("Zooming")]
		[SerializeField]	float		m_ZoomInScale			=	2f;
		[SerializeField]	float		m_ZoomOutScale			=	1f;
		
		[Header("Interactive")]
		[SerializeField]	float		m_TouchDuration			=	3f;
		[SerializeField]	RawImage	m_PostInteractiveImage;
		
		//---------------------------------------------------------------------
		
		private				Vector3		m_StartCache;
		private				Vector3		m_EndCache;
		private				bool		m_Touchable				=	false;
		private				float		m_CurrentTouchDuration	=	0f;
		
		//---------------------------------------------------------------------

		protected override void Start()
		{
			base.Start();
			
			m_StartCache		=	m_Content.localScale;
			m_EndCache			=	m_Content.localScale + Vector3.one * (m_ZoomInScale - m_ZoomOutScale);
			
		}
		//---------------------------------------------------------------------

		protected override void OnFadeInProgress(float _progress)
		{
			base.OnFadeInProgress(_progress);
			
			var diffScale			=	m_ZoomInScale - m_ZoomOutScale;
			m_Content.localScale	=	m_StartCache + Vector3.one * (m_ZoomInScale - m_ZoomOutScale) * +_progress;
		}
		
		//---------------------------------------------------------------------

		protected override void OnFadeOutProgress(float _progress)
		{
			base.OnFadeOutProgress(_progress);
			
			var diffScale			=	m_ZoomInScale - m_ZoomOutScale;
			m_Content.localScale	=	m_EndCache - Vector3.one * (m_ZoomInScale - m_ZoomOutScale) * + (1 - _progress);
		}
		
		//---------------------------------------------------------------------

		public override void Show(bool _force = false)
		{
			if(m_FadeDisposable != null)
				m_FadeDisposable.Dispose();
			
			m_CanvasGroup.blocksRaycasts	=	false;
			m_CanvasGroup.interactable		=	false;
			m_Content.gameObject.SetActive(true);
			
			if (_force)
			{
				m_CanvasGroup.alpha			=	1f;
				OnShow();
			}
			else
			{
				if(m_FadeDisposable != null)
					m_FadeDisposable.Dispose();
				
				m_FadeDisposable				=	Fader.Animate
				(
					m_CanvasGroup.alpha,
					1f,
					m_FadeInDuration,
					Easing.Type.EaseOutExpo,
					OnFadeInProgress,
					OnShow
				);
			}
			
			UIController.instance.SetBackgroundColor(m_BackgroundColor);
		}
		
		//---------------------------------------------------------------------

		protected override void OnShow()
		{
			base.OnShow();
			TouchParticleController.instance.ToggleVisible(true);
			m_CurrentTouchDuration	=	0f;
			m_Touchable				=	true;
		}

		//---------------------------------------------------------------------

		public override void Hide(bool _force = false)
		{
			TouchParticleController.instance.ToggleVisible(false);
			
			m_ForceHideEarlier				=	_force;
			m_CanvasGroup.interactable		=	false;
			m_CanvasGroup.blocksRaycasts	=	false;
			if (_force)
			{
				m_CanvasGroup.alpha			=	0f;
				OnHide();
			}
			else
			{
				if(m_FadeDisposable != null)
					m_FadeDisposable.Dispose();
				
				m_FadeDisposable				=	Fader.Animate
				(
					m_CanvasGroup.alpha,
					0f,
					m_FadeOutDuration,
					Easing.Type.EaseOutCirc,
					OnFadeOutProgress,
					OnHide
				);
			}
		}

		//---------------------------------------------------------------------

		protected override void OnHide()
		{
			base.OnHide();
			
			m_PostInteractiveImage.color	=	new Color
			(
				m_PostInteractiveImage.color.r,
				m_PostInteractiveImage.color.g,
				m_PostInteractiveImage.color.b,
				0f
			);
		}
		
		//---------------------------------------------------------------------

		private void LateUpdate()
		{
#if UNITY_EDITOR
			if (!Input.GetMouseButton(0) || !m_Touchable)
#elif UNITY_ANDROID || UNITY_IOS
			if(Input.touches.Length == 0 || !m_Touchable)
#endif
				return;
			
			m_CurrentTouchDuration	+=	Time.deltaTime;
			
			m_PostInteractiveImage.color	=	new Color
			(
				m_PostInteractiveImage.color.r,
				m_PostInteractiveImage.color.g,
				m_PostInteractiveImage.color.b,
				m_CurrentTouchDuration / m_TouchDuration
			);

			if (m_CurrentTouchDuration >= m_TouchDuration)
			{
				m_Touchable	=	false;
				GoNext();
			}
		}

		//---------------------------------------------------------------------
	}

	//---------------------------------------------------------------------
}