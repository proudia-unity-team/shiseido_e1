﻿using System.Linq;
using UnityEngine;
using Utilities;

namespace Controller
{
	//---------------------------------------------------------------------

	public class TouchParticleController : Singleton<TouchParticleController>
	{
		//---------------------------------------------------------------------
		
		[SerializeField]	Camera			m_ParticleCamera;
		[SerializeField]	ParticleSystem	m_TouchParticle;
		[SerializeField]	ParticleSystem	m_PopParticleRef;

		//---------------------------------------------------------------------
		
		private 			bool			m_Visible			=	false;
		private 			bool			m_Spawning			=	false;
		private 			Vector3			m_CurrentPosition;
		private				ParticleSystem	m_CurrentParticle;
		
		//---------------------------------------------------------------------

		private void Start()
		{
			m_Spawning	=	false;
		}

		//---------------------------------------------------------------------

		public void ToggleVisible(bool _visible)
		{
			m_Visible	=	_visible;
		}
		
		//---------------------------------------------------------------------
		
		private void Update()
		{
			if (!m_Visible || m_ParticleCamera == null || m_TouchParticle == null || !Input.GetMouseButton(0))
			{
				m_Spawning					=	false;
				if(m_CurrentParticle != null)
				{
					m_CurrentParticle.Stop();			
				}
				m_CurrentParticle			=	null;
				//m_TouchParticle.gameObject.SetActive(false);
				return;
			}
		
			var	touchPosition				=	m_ParticleCamera.ScreenToWorldPoint(Input.mousePosition);
			m_CurrentPosition 				= 	new Vector3
			(
				touchPosition.x,
				touchPosition.y,
				50f
			);
			
			if(m_CurrentParticle != null)
				m_CurrentParticle.transform.position = m_CurrentPosition;
		}

		//---------------------------------------------------------------------

		private void LateUpdate()
		{
			var	emission		=	m_TouchParticle.emission;
			
			if (!m_Visible || m_ParticleCamera == null || m_TouchParticle == null || !Input.GetMouseButton(0))
				return;
			
			emission.enabled			=	true;
			
			if (!m_Spawning)
			{
				//m_TouchParticle.gameObject.SetActive(true);
				m_CurrentParticle			=	Instantiate
				(
					m_TouchParticle,
					m_CurrentPosition,
					Quaternion.identity
				);
				
				m_Spawning					=	true;
				Instantiate
				(
					m_PopParticleRef,
					m_CurrentPosition,
					Quaternion.identity
				);
			}
		}

		//---------------------------------------------------------------------
	}
	
	//---------------------------------------------------------------------
}