﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI.Content
{
	//---------------------------------------------------------------------

	public class SlideShowContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[Header("Fade")]
		[SerializeField]	float			m_SlideFadeDuration	=	1.5f;

		[Header("Skins")]
		[SerializeField]	RawImage		m_SkinImage1;
		[SerializeField]	RawImage		m_SkinImage2;
		
		//---------------------------------------------------------------------
		
		private 			IDisposable		m_Fade01Disposable;
		private 			IDisposable		m_Fade02Disposable;
		
		//---------------------------------------------------------------------

		public override void Show(bool _force = false)
		{
			Reset();
			base.Show(_force);
		}
		//---------------------------------------------------------------------

		protected override void OnShow()
		{
			base.OnShow();
			
			if(m_Fade01Disposable != null)
				m_Fade01Disposable.Dispose();
			if(m_Fade02Disposable != null)
				m_Fade02Disposable.Dispose();
			
			m_Fade01Disposable	=	Fader.Animate
			(
				0f,
				1f,
				m_SlideFadeDuration,
				_progress =>
				{
					m_SkinImage1.color	=	new Color
					(
						m_SkinImage1.color.r,
						m_SkinImage1.color.g,
						m_SkinImage1.color.b,
						1f - _progress
					);
					
					m_SkinImage2.color		=new Color
					(
						m_SkinImage1.color.r,
						m_SkinImage1.color.g,
						m_SkinImage1.color.b,
						_progress
					);
				},
				() => GoNext()
			);
		}

		//---------------------------------------------------------------------

		protected override void OnHide()
		{
			base.OnHide();
			Reset();
		}

		//---------------------------------------------------------------------

		private void Reset()
		{
			m_SkinImage1.color	=	new Color
			(
				m_SkinImage1.color.r,
				m_SkinImage1.color.g,
				m_SkinImage1.color.b,
				1f
			);
			
			m_SkinImage2.color	=	new Color
			(
				m_SkinImage2.color.r,
				m_SkinImage2.color.g,
				m_SkinImage2.color.b,
				0f
			);
		}
		
		//---------------------------------------------------------------------
	}

	//---------------------------------------------------------------------
}