﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI.Content
{
	//---------------------------------------------------------------------

	public class TimedContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[SerializeField]	float			m_LoadDuration	=	2.5f;
		[SerializeField]	Image			m_Progress;
		
		//---------------------------------------------------------------------
		
		private				IDisposable		m_CountdownDisposable;
		
		//---------------------------------------------------------------------

		protected override void BackHome()
		{
			if(m_CountdownDisposable != null)
				m_CountdownDisposable.Dispose();
						
			base.BackHome();
		}
		
		//---------------------------------------------------------------------

		protected override void OnShow()
		{
			base.OnShow();
			
			if(m_CountdownDisposable != null)
				m_CountdownDisposable.Dispose();
			
			m_CountdownDisposable	=	Fader.Animate
			(
				0f,
				1f,
				m_LoadDuration,
				_progress =>
				{
					m_Progress.fillAmount	=	_progress;
				},
				() =>
				{
					if(!GoNext())
						GoPrevious();
				}
			);
		}
		
		//---------------------------------------------------------------------
	}

	//---------------------------------------------------------------------
}