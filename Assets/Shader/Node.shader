// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:True,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33466,y:32666,varname:node_3138,prsc:2|emission-3083-OUT,alpha-6784-OUT,clip-6784-OUT;n:type:ShaderForge.SFN_TexCoord,id:7466,x:31108,y:32726,varname:node_7466,prsc:2,uv:0,uaff:True;n:type:ShaderForge.SFN_ComponentMask,id:2912,x:31349,y:32581,varname:node_2912,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-7466-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:3238,x:31350,y:32867,varname:node_3238,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7466-UVOUT;n:type:ShaderForge.SFN_Multiply,id:4408,x:31566,y:32685,varname:node_4408,prsc:2|A-2912-OUT,B-1869-OUT;n:type:ShaderForge.SFN_Vector1,id:1869,x:31350,y:32770,varname:node_1869,prsc:2,v1:3;n:type:ShaderForge.SFN_Multiply,id:4484,x:31566,y:32833,varname:node_4484,prsc:2|A-3238-OUT,B-1869-OUT;n:type:ShaderForge.SFN_Sin,id:8998,x:31764,y:32685,varname:node_8998,prsc:2|IN-4408-OUT;n:type:ShaderForge.SFN_Sin,id:5249,x:31764,y:32833,varname:node_5249,prsc:2|IN-4484-OUT;n:type:ShaderForge.SFN_Power,id:7790,x:32260,y:32685,varname:node_7790,prsc:2|VAL-1690-OUT,EXP-6418-OUT;n:type:ShaderForge.SFN_Power,id:5062,x:32260,y:32831,varname:node_5062,prsc:2|VAL-4701-OUT,EXP-6418-OUT;n:type:ShaderForge.SFN_OneMinus,id:1690,x:31943,y:32685,varname:node_1690,prsc:2|IN-8998-OUT;n:type:ShaderForge.SFN_OneMinus,id:4701,x:31943,y:32833,varname:node_4701,prsc:2|IN-5249-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6418,x:32069,y:32788,ptovrint:False,ptlb:Edge sharpness,ptin:_Edgesharpness,varname:node_6418,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:7868,x:32414,y:32733,ptovrint:False,ptlb:Edge substraction,ptin:_Edgesubstraction,varname:_Edgesharpness_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Subtract,id:8906,x:32607,y:32634,varname:node_8906,prsc:2|A-7790-OUT,B-7868-OUT;n:type:ShaderForge.SFN_Subtract,id:2768,x:32607,y:32785,varname:node_2768,prsc:2|A-5062-OUT,B-7868-OUT;n:type:ShaderForge.SFN_Lerp,id:6784,x:32817,y:32765,varname:node_6784,prsc:2|A-8906-OUT,B-2768-OUT,T-7654-OUT;n:type:ShaderForge.SFN_Vector1,id:7654,x:32607,y:32942,varname:node_7654,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:6666,x:33062,y:32765,varname:node_6666,prsc:2|A-6784-OUT,B-661-RGB;n:type:ShaderForge.SFN_Color,id:661,x:32817,y:32954,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_661,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:1164,x:33062,y:32927,ptovrint:False,ptlb:Grow strength,ptin:_Growstrength,varname:node_1164,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:3083,x:33252,y:32765,varname:node_3083,prsc:2|A-6666-OUT,B-1164-OUT;proporder:6418-7868-661-1164;pass:END;sub:END;*/

Shader "Shader Forge/Node" {
    Properties {
        _Edgesharpness ("Edge sharpness", Float ) = 0.5
        _Edgesubstraction ("Edge substraction", Float ) = 0.5
        _Color ("Color", Color) = (0,1,1,1)
        _Growstrength ("Grow strength", Float ) = 2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Edgesharpness;
            uniform float _Edgesubstraction;
            uniform float4 _Color;
            uniform float _Growstrength;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float node_1869 = 3.0;
                float node_6784 = lerp((pow((1.0 - sin((i.uv0.g*node_1869))),_Edgesharpness)-_Edgesubstraction),(pow((1.0 - sin((i.uv0.r*node_1869))),_Edgesharpness)-_Edgesubstraction),0.5);
                clip(node_6784 - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = ((node_6784*_Color.rgb)*_Growstrength);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_6784);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Edgesharpness;
            uniform float _Edgesubstraction;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_1869 = 3.0;
                float node_6784 = lerp((pow((1.0 - sin((i.uv0.g*node_1869))),_Edgesharpness)-_Edgesubstraction),(pow((1.0 - sin((i.uv0.r*node_1869))),_Edgesharpness)-_Edgesubstraction),0.5);
                clip(node_6784 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
