﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using UIContent = UI.UIContent;

namespace UI.Content
{
	//---------------------------------------------------------------------

	public class SummarizeContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[SerializeField]	float				m_SummaryFadeDuration	=	1.5f;
		[SerializeField]	Button				m_DetailButton;
		[SerializeField]	CanvasGroup			m_VideoCG;
		[SerializeField]	CanvasGroup			m_SummaryCG;
		[SerializeField]	MediaPlayerCtrl		m_MediaPlayerCtrl;
		
		//---------------------------------------------------------------------

		protected override void Start()
		{
			base.Start();
			
			if(m_DetailButton != null)
				m_DetailButton.OnClickAsObservable().Subscribe
				(
					_ =>
					{
						Application.OpenURL("ssdsba://line?line_id=39&line_step=9");
					}
				);
		}

		//---------------------------------------------------------------------

		private void OnEnable()
		{
			m_MediaPlayerCtrl.OnEnd	+=	OnVideoEnd;
		}

		//---------------------------------------------------------------------

		private void OnDisable()
		{
			m_MediaPlayerCtrl.OnEnd	-=	OnVideoEnd;
		}
		
		//---------------------------------------------------------------------

		private void OnVideoEnd()
		{
			Fader.Animate
			(
				0f,
				1f,
				m_SummaryFadeDuration,
				_progress => m_SummaryCG.alpha	=	_progress,
				() =>
				{
					m_SummaryCG.interactable	=	true;
					m_SummaryCG.blocksRaycasts	=	true;
				}
			);
		}
		
		//---------------------------------------------------------------------

		public override void Show(bool _force = false)
		{
			if(m_FadeDisposable != null)
				m_FadeDisposable.Dispose();
			
			m_CanvasGroup.blocksRaycasts	=	false;
			m_CanvasGroup.interactable		=	false;
			m_Content.gameObject.SetActive(true);

			if (_force)
			{
				m_CanvasGroup.alpha			=	1f;
				OnShow();
			}
			else
			{
				if(m_FadeDisposable != null)
					m_FadeDisposable.Dispose();
				
				m_FadeDisposable				=	Fader.Animate
				(
					m_CanvasGroup.alpha,
					1f,
					m_FadeInDuration,
					_progress => m_CanvasGroup.alpha	=	_progress,
					OnShow
				);
			}
			
			UIController.instance.SetBackgroundColor(m_BackgroundColor);
		}
		
		//---------------------------------------------------------------------

		protected override void OnHide()
		{
			m_VideoCG.alpha				=	1f;
			m_SummaryCG.alpha			=	0f;
			m_SummaryCG.interactable	=	false;
			m_SummaryCG.blocksRaycasts	=	false;
			
			base.OnHide();
		}

		//---------------------------------------------------------------------
	}

	//---------------------------------------------------------------------
}