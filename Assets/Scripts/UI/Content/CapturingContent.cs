﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI.Content
{
	//---------------------------------------------------------------------

	public class CapturingContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[Header("Capturing")]
		[SerializeField]	Image				m_Progress;
		[SerializeField]	float				m_LoadDuration	=	2.5f;
		
		[Header("Hiding")]
		[SerializeField]	float				m_HidingScale	=	0.5f;
		
		[Header("Canvas Group")]
		[SerializeField]	CanvasGroup			m_CapturingCG;
		[SerializeField]	CanvasGroup			m_PreviewCG;
		
		//---------------------------------------------------------------------
		
		private				IDisposable			m_PreviewDisposable;
		private				IDisposable			m_PreviewCountdownDisposable;
		private				IDisposable			m_CountdownDisposable;
		
		//---------------------------------------------------------------------

		public override void Show(bool _force = false)
		{
			Reset();
			base.Show(_force);
		}
		//---------------------------------------------------------------------

		protected override void OnShow()
		{
			m_CapturingCG.interactable		=	true;
			m_CapturingCG.blocksRaycasts	=	true;
			
			if(m_CountdownDisposable != null)
				m_CountdownDisposable.Dispose();
			
			m_CountdownDisposable	=	Fader.Animate
			(
				0f,
				1f,
				m_LoadDuration,
				_progress =>
				{
					m_Progress.fillAmount	=	_progress;
				},
				() =>
				{
					m_CapturingCG.interactable		=	false;
					m_CapturingCG.blocksRaycasts	=	false;
			
					if(m_PreviewDisposable != null)
						m_PreviewDisposable.Dispose();
			
					m_PreviewDisposable	=	Fader.Animate
					(
						0f,
						1f,
						m_LoadDuration,
						_progress =>
						{
							m_CapturingCG.alpha		=	1f - _progress;
							m_PreviewCG.alpha		=	_progress;
						},
						ShowPreview
					);
				}
			);
		}
		//---------------------------------------------------------------------

		protected override void OnFadeOutProgress(float _progress)
		{
			base.OnFadeOutProgress(_progress);
			
			m_Content.localScale	=	new Vector3
			(
				1f + (m_HidingScale * (1f - _progress)),
				1f + (m_HidingScale * (1f - _progress)),
				1f + (m_HidingScale * (1f - _progress))
			);
		}
		
		//---------------------------------------------------------------------

		private void ShowPreview()
		{
			if(m_PreviewCountdownDisposable != null)
				m_PreviewCountdownDisposable.Dispose();
			
			m_PreviewCountdownDisposable	=	Fader.Animate
			(
				0f,
				1f,
				m_LoadDuration,
				_progress => {},
				() =>
				{
					if(!GoNext())
						GoPrevious();
				}
			);
		}
		
		//---------------------------------------------------------------------

		protected override void OnHide()
		{
			base.OnHide();
			Reset();
		}
		
		//---------------------------------------------------------------------

		private void Reset()
		{
			m_Content.localScale			=	Vector3.one;
			m_Progress.fillAmount			=	0f;
			
			m_CapturingCG.alpha				=	1f;
			m_CapturingCG.interactable		=	false;
			m_CapturingCG.blocksRaycasts	=	false;
			
			m_PreviewCG.alpha				=	0f;
			m_PreviewCG.interactable		=	false;
			m_PreviewCG.blocksRaycasts		=	false;
		}

		//---------------------------------------------------------------------
	}

	//---------------------------------------------------------------------
}