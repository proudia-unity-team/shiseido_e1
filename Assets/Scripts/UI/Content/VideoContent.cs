﻿using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI.Content
{
	public class VideoContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[SerializeField]	MediaPlayerCtrl		m_MediaPlayerCtrl;
		
		//---------------------------------------------------------------------
		
		private void OnEnable()
		{
			m_MediaPlayerCtrl.OnEnd	+=	OnVideoEnd;
		}

		//---------------------------------------------------------------------

		private void OnDisable()
		{
			m_MediaPlayerCtrl.OnEnd	-=	OnVideoEnd;
		}
		
		//---------------------------------------------------------------------

		private void OnVideoEnd()
		{
			GoNext();
		}
		
		//---------------------------------------------------------------------
	}
}