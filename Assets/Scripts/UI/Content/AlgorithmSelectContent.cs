﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Content
{
	//---------------------------------------------------------------------

	public class AlgorithmSelectContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[SerializeField]	Button		m_PhotoButton;
		[SerializeField]	Button		m_FingerButton;
		
		//---------------------------------------------------------------------

		protected override void Start()
		{
			base.Start();

			if (m_PhotoButton != null)
			{
				m_PhotoButton.OnClickAsObservable().Subscribe
				(
					_ =>
					{
						m_NextContent			=	UIController.instance.GetContent(UIState.Warning);
						GoNext();
					}
				);
			}
			
			if (m_FingerButton != null)
			{
				m_FingerButton.OnClickAsObservable().Subscribe
				(
					_ =>
					{
						m_NextContent			=	UIController.instance.GetContent(UIState.HandTouch);
						GoNext();
					}
				);
			}
		}

		//---------------------------------------------------------------------
	}
	
	//---------------------------------------------------------------------
}