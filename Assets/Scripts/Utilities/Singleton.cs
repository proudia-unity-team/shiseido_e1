﻿using UnityEngine;

namespace Utilities
{
	public class Singleton<T> : MonoBehaviour
	{
		//-----------------------------------------------------------------

		public static T instance;
	
		//-----------------------------------------------------------------

		protected virtual void Awake()
		{
			instance    =   this.GetComponent<T>();
		}
	
		//-----------------------------------------------------------------
	}
}