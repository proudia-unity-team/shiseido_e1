﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI
{
	//---------------------------------------------------------------------
		
	public enum BackgroundColor
	{
		White	=	0,
		Black	=	1,
	}
	
	//---------------------------------------------------------------------

	public class BackgroundContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[Header("Duration")]
		[SerializeField]	float		m_SwitchDuration	=	1f;

		[Header("BG Image")]
		[SerializeField]	Image		m_WhiteBG;
		[SerializeField]	Image		m_BlackBG;
		
		//---------------------------------------------------------------------
		
		private				IDisposable	m_FadeDisposable;
		
		//---------------------------------------------------------------------

		public override void Init(int _sortOrder, bool _visible, UIContent _next, UIContent _previous)
		{
			base.Init(_sortOrder, _visible, _next, _previous);
				
			m_WhiteBG.enabled	=	true;
			m_BlackBG.enabled	=	true;
			
			m_WhiteBG.color		=	new Color
			(
				m_WhiteBG.color.r,
				m_WhiteBG.color.g,
				m_WhiteBG.color.b,
			1f
			);
			m_BlackBG.color		=	new Color
			(
				m_BlackBG.color.r,
				m_BlackBG.color.g,
				m_BlackBG.color.b,
				0f
			);
		}

		//---------------------------------------------------------------------

		public void SetBackgroundColor(BackgroundColor _color)
		{
			if(m_FadeDisposable != null)
				m_FadeDisposable.Dispose();
			
			m_FadeDisposable	=	Fader.Animate
			(
				_color == BackgroundColor.White ? m_WhiteBG.color.a : m_BlackBG.color.a,
				1f,
				m_SwitchDuration,
				_progress =>
				{
					m_WhiteBG.color		=	new Color
					(
						m_WhiteBG.color.r,
						m_WhiteBG.color.g,
						m_WhiteBG.color.b,
						_color == BackgroundColor.White ? _progress : 1f - _progress
					);
					m_BlackBG.color		=	new Color
					(
						m_BlackBG.color.r,
						m_BlackBG.color.g,
						m_BlackBG.color.b,
						_color == BackgroundColor.Black ? _progress : 1f - _progress
					);
				}
			);
		}
		
		//---------------------------------------------------------------------
	}
	
	//---------------------------------------------------------------------
}