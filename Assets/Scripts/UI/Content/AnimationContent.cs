﻿using System;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Content
{
	//---------------------------------------------------------------------

	[RequireComponent(typeof(Animator))]
	public class AnimationContent : UIContent
	{
		//---------------------------------------------------------------------
		
		[SerializeField]		string				m_TriggerName;
		[SerializeField]		string				m_DefaultTriggerName;
		[SerializeField]		Button				m_DetailButton;
		
		//---------------------------------------------------------------------

		protected override void Start()
		{
			base.Start();
			
			if(m_DetailButton != null)
				m_DetailButton.OnClickAsObservable().Subscribe
				(
					_ =>
					{
						Application.OpenURL("http://www.shiseido.co.jp/");
					}
				);
		}
		
		//---------------------------------------------------------------------

		public override void Show(bool _forced = false)
		{
			base.Show(_forced);
		}
		
		//---------------------------------------------------------------------

		protected override void OnShow()
		{
			base.OnShow();
			GetComponent<Animator>().SetTrigger(m_TriggerName);
		}
		//---------------------------------------------------------------------

		protected override void OnHide()
		{
			base.OnHide();
			GetComponent<Animator>().SetTrigger(m_DefaultTriggerName);
		}
		
		//---------------------------------------------------------------------
	}

	//---------------------------------------------------------------------
}