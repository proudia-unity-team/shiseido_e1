﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(ParticleSystem))]
public class ParticleAttractor : MonoBehaviour
{
    ParticleSystem ps;
    ParticleSystem.Particle[] m_Particles;

    public float speed = 5f;
    int numParticlesAlive;
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        if (!GetComponent<Transform>())
        {
            GetComponent<Transform>();
        }
    }
    void Update()
    {
		CheckParticleAttracting();
    }

    void CheckParticleAttracting()
    {
        List<GameObject> nearPointList = new List<GameObject>();
        bool hasMagnet = false;
        for (int i = 0; i < NervePointManager.instance.nerveList.Count; i++)
        {
            float distance = Vector3.Distance(NervePointManager.instance.nerveList[i].transform.position, this.transform.position);
            if (distance < 2)
            {
                hasMagnet = true;
                nearPointList.Add(NervePointManager.instance.nerveList[i]);
            }
        }
        if (hasMagnet)
        {
            m_Particles = new ParticleSystem.Particle[ps.main.maxParticles];
            numParticlesAlive = ps.GetParticles(m_Particles);
            float step = speed * Time.deltaTime;
            int pointOrder = 0;
            for (int i = 0; i < numParticlesAlive; i++)
            {
                m_Particles[i].position = Vector3.LerpUnclamped(m_Particles[i].position, nearPointList[pointOrder].transform.position, step);
                pointOrder++;
                if (pointOrder == nearPointList.Count)
                    pointOrder = 0;
            }
            ps.SetParticles(m_Particles, numParticlesAlive);
        }
    }
}
