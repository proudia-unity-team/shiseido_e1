﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerformanceDebug : MonoBehaviour 
{
	//-----------------------------------------------------------
	
	[SerializeField]	Transform	m_CameraTransform;
	[SerializeField]	Vector2		m_ZoomRange				=	new Vector2(-.9f, -0.05f);
	
	//-----------------------------------------------------------

	public void OnSliderValueChanged(float _value)
	{
		m_CameraTransform.position	=	new Vector3
		(
			m_CameraTransform.position.x,
			m_CameraTransform.position.y,
			m_ZoomRange.x + Mathf.Abs(m_ZoomRange.y - m_ZoomRange.x) * _value
		);
	}
	
	//-----------------------------------------------------------
}
