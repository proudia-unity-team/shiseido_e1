﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI
{
	//---------------------------------------------------------------------
	
	[RequireComponent(typeof(Canvas))]
	[RequireComponent(typeof(CanvasGroup))]
	public class UIContent : MonoBehaviour
	{
		//---------------------------------------------------------------------
		
		[Header("Content")]
		[SerializeField]	protected	UIState				m_UIState;
		[SerializeField]	protected	BackgroundColor		m_BackgroundColor	=	BackgroundColor.White;
		[SerializeField]	protected	Transform			m_Content;
		[SerializeField] 	protected 	Button				m_HomeButton;
		
		[Header("Fading")]
		[SerializeField]	protected	bool				m_CrossFade			=	true;
		[SerializeField]	protected	float				m_FadeInDuration	=	1f;
		[SerializeField]	protected	float				m_FadeOutDuration	=	1f;
		
		//---------------------------------------------------------------------
		
		protected 						bool				m_ShowNext			=	true;
		protected 						bool				m_ForceHideEarlier	=	false;
		protected 						IDisposable			m_FadeDisposable;
		protected						UIContent			m_NextContent;
		protected						UIContent			m_PreviousContent;
		
		//---------------------------------------------------------------------

		private 						CanvasGroup 		m_CanvasGroupInstance;
				
		protected 						CanvasGroup 		m_CanvasGroup
		{
			get
			{
				if(m_CanvasGroupInstance == null)
					m_CanvasGroupInstance = GetComponent<CanvasGroup>();
				
				return m_CanvasGroupInstance;
			}
		}
		
		//---------------------------------------------------------------------

		protected virtual void Start()
		{
			if(m_HomeButton != null)
				m_HomeButton.OnClickAsObservable().Subscribe
				(
					_ =>
					{
						BackHome();
					}
				);
		}
		
		//---------------------------------------------------------------------

		public UIState GetState()
		{
			return m_UIState;
		}
		
		//---------------------------------------------------------------------

		public virtual void Init(int _sortOrder, bool _visible, UIContent _next, UIContent _previous)
		{
			GetComponent<Canvas>().sortingOrder	=	_sortOrder;
			
			switch (m_UIState)
			{
				case UIState.Warning:
			
					m_NextContent			=	UIController.instance.GetContent(UIState.TakePhoto);
					m_PreviousContent		=	UIController.instance.GetContent(UIState.Selection);
					break;
					
				case UIState.TakePhoto:
			
					m_NextContent			=	UIController.instance.GetContent(UIState.Interactive);
					m_PreviousContent		=	UIController.instance.GetContent(UIState.Warning);
					break;
					
				case UIState.HandTouch:
			
					m_NextContent			=	UIController.instance.GetContent(UIState.Interactive);
					m_PreviousContent		=	UIController.instance.GetContent(UIState.Selection);
					break;
					
				case UIState.Interactive:
			
					m_NextContent			=	UIController.instance.GetContent(UIState.Summary);
					m_PreviousContent		=	UIController.instance.GetContent(UIState.Selection);
					break;
					
				case UIState.Summary:
			
					m_NextContent			=	UIController.instance.GetContent(UIState.Standby);
					m_PreviousContent		=	UIController.instance.GetContent(UIState.Selection);
					break;
					
				default:
					
					m_NextContent			=	_next;
					m_PreviousContent		=	_previous;
					break;
			}
			
			if(_visible) Show(true); else Hide(true);
		}
		
		//---------------------------------------------------------------------

		public virtual void Show(bool _force = false)
		{
			if(m_FadeDisposable != null)
				m_FadeDisposable.Dispose();
			
			m_CanvasGroup.blocksRaycasts	=	false;
			m_CanvasGroup.interactable		=	false;
			m_Content.gameObject.SetActive(true);
			
			if (_force)
			{
				m_CanvasGroup.alpha			=	1f;
				OnShow();
			}
			else
			{
				if(m_FadeDisposable != null)
					m_FadeDisposable.Dispose();
				
				m_FadeDisposable				=	Fader.Animate
				(
					m_CanvasGroup.alpha,
					1f,
					m_FadeInDuration,
					OnFadeInProgress,
					OnShow
				);
			}
			
			UIController.instance.SetBackgroundColor(m_BackgroundColor);
		}
		
		//---------------------------------------------------------------------

		protected virtual void OnFadeInProgress(float _progress)
		{
			m_CanvasGroup.alpha	=	_progress;
		}
		
		//---------------------------------------------------------------------

		protected virtual void OnFadeOutProgress(float _progress)
		{
			m_CanvasGroup.alpha	=	_progress;
		}
		
		//---------------------------------------------------------------------

		protected virtual void OnShow()
		{
			m_CanvasGroup.blocksRaycasts	=	true;
			m_CanvasGroup.interactable		=	true;
		}
		
		//---------------------------------------------------------------------

		[ContextMenu("Hide")]
		public virtual void Hide(bool _force = false)
		{
			m_ForceHideEarlier				=	_force;
			m_CanvasGroup.interactable		=	false;
			m_CanvasGroup.blocksRaycasts	=	false;

			if (_force)
			{
				m_CanvasGroup.alpha			=	0f;
				OnHide();
			}
			else
			{
				if(m_FadeDisposable != null)
					m_FadeDisposable.Dispose();
				
				m_FadeDisposable				=	Fader.Animate
				(
					m_CanvasGroup.alpha,
					0f,
					m_FadeOutDuration,
					OnFadeOutProgress,
					OnHide
				);
			}
		}
		
		//---------------------------------------------------------------------

		protected virtual void OnHide()
		{
			m_CanvasGroup.alpha				=	0f;
			m_CanvasGroup.interactable		=	false;
			m_CanvasGroup.blocksRaycasts	=	false;
			
			m_Content.gameObject.SetActive(false);

			if (!m_CrossFade && !m_ForceHideEarlier)
			{
				if(m_ShowNext)
					m_NextContent.Show();
				else
					m_PreviousContent.Show();
			}
		}
		
		//---------------------------------------------------------------------

		[ContextMenu("Init Conent")]
		public virtual void InitContent()
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				if(!transform.GetChild(i).name.Contains("Content"))
					continue;
				
				m_Content	=	transform.GetChild(i);
				break;
			}
		}
		
		//---------------------------------------------------------------------
		
		public virtual bool GoNext()
		{
			if(m_NextContent == null)
				return false;
			
			Hide();
			
			m_ShowNext	=	true;
			
			if(m_CrossFade)
				m_NextContent.Show();
			
			return true;
		}
		
		//---------------------------------------------------------------------

		public virtual bool GoPrevious()
		{
			if(m_PreviousContent == null)
				return false;
			
			Hide();
			
			m_ShowNext	=	false;

			if(m_CrossFade)
				m_PreviousContent.Show();
			
			return true;
		}
		
		//---------------------------------------------------------------------

		protected virtual void BackHome()
		{
			UIController.instance.Reset();
		}
		
		//---------------------------------------------------------------------
	}
	
	//---------------------------------------------------------------------
}
